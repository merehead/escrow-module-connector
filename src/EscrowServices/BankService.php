<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait BankService
{
    public function getBanks(int $countryId)
    {
        $body = [
            'country_id' => $countryId,
        ];

        return $this->makeCallGuzzle('GET', 'banks', $body);
    }
}
