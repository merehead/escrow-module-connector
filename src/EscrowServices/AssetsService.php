<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait AssetsService
{
    public function getAssets(bool $makeVisible = false)
    {
        $body = [
            'make_visible' => $makeVisible,
        ];

        return $this->makeCallGuzzle('GET', 'assets', $body);
    }

    public function getFiatPrice(int $assetId, string $fiatCode, float $margin = 100, float $amount = 1)
    {
        $body = [
            'asset_id'  => $assetId,
            'fiat_code' => $fiatCode,
            'margin'    => $margin,
            'amount'    => $amount,
        ];

        return $this->makeCallGuzzle('GET', 'fiat_price', $body);
    }

    public function getProposalPrice(int $countryId, string $type, int $assetId, ?float $margin = 100, ?float $amount = 1)
    {
        $body = [
            'country_id' => $countryId,
            'type'       => $type,
            'asset_id'   => $assetId,
            'margin'     => $margin,
            'amount'     => $amount,
        ];

        return $this->makeCallGuzzle('GET', 'proposal_price', $body);
    }

    public function getExchangePrice(int $advertisementId, string $type, int $assetId, ?float $amount = 1)
    {
        $body = [
            'advertisement_id' => $advertisementId,
            'type'             => $type,
            'amount'           => $amount,
        ];

        return $this->makeCallGuzzle('GET', 'exchange_price', $body);
    }

    public function updateAsset(array $body)
    {
        return $this->makeCallGuzzle('PUT', 'asset', $body);
    }
}
