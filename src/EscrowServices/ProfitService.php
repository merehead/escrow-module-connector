<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait ProfitService
{
    public function getTotalProfit(int $assetId)
    {
        $body = [
            'asset_id' => $assetId,
        ];

        return $this->makeCallGuzzle('GET', 'total_profit', $body);
    }

    public function getProfit(int $assetId)
    {
        $body = [
            'asset_id' => $assetId,
        ];

        return $this->makeCallGuzzle('GET', 'profit', $body);
    }
}
