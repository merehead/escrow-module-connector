<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait UserService
{
    public function createAccount(int $userId, int $parentId = null)
    {
        $body = [
            'id'        => $userId,
            'parent_id' => $parentId,
        ];

        return $this->makeCallGuzzle('POST', 'account', $body);
    }

    public function changeVerifyStatus(int $userId, bool $verified): array
    {
        $body = [
            'user_id'  => $userId,
            'verified' => $verified,
        ];

        return $this->makeCallGuzzle('PUT', 'change_verify_status', $body);
    }

    public function getTradingStatisticsByUser(int $userId)
    {
        // Below is a crutch to prevent strange behavior of Guzzle. ( 403 Forbidden )
        // ||
        // ||
        // \/

        $curl = curl_init();
        $baseUrl = config('EscrowModuleConnector.escrowModuleDns');

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if (config('EscrowModuleConnector.encryption_key')) {
            $data = $this->encrypter->encrypt(json_encode(['user_id' => $userId]));
            curl_setopt($curl, CURLOPT_URL, $baseUrl . 'user_trading_statistics?encrypted_data='.$data);
        } else {
            curl_setopt($curl, CURLOPT_URL, $baseUrl . 'user_trading_statistics?user_id='.$userId);
        }

        $output = curl_exec($curl);
        curl_close($curl);

        return json_decode($output, 1);

        // Original code
//        $body = [
//            'user_id'  => $userId
//        ];
//
//        return $this->makeCallGuzzle('GET', 'user_trading_statistics', $body);
    }
}
