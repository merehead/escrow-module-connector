<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait AdvertisementTimerService
{
    public function getAdvertisementTimers(int $userId, int $advertisementId)
    {
        $body = [
            'user_id'          => $userId,
            'advertisement_id' => $advertisementId,
        ];

        return $this->makeCallGuzzle('GET', 'advertisement_timers', $body);
    }

    public function createAdvertisementTimer(int $userId, int $advertisementId, string $startAt, string $endAt)
    {
        $body = [
            'user_id'          => $userId,
            'advertisement_id' => $advertisementId,
            'start_at'         => $startAt,
            'end_at'           => $endAt,
        ];

        return $this->makeCallGuzzle('POST', 'advertisement_timer', $body);
    }

    public function updateAdvertisementTimer(int $userId, int $advertisementTimerId, string $startAt, string $endAt)
    {
        $body = [
            'user_id'                => $userId,
            'advertisement_timer_id' => $advertisementTimerId,
            'start_at'               => $startAt,
            'end_at'                 => $endAt,
        ];

        return $this->makeCallGuzzle('PUT', 'advertisement_timer', $body);
    }

    public function deleteAdvertisementTimer(int $userId, int $advertisementTimerId)
    {
        $body = [
            'user_id'                => $userId,
            'advertisement_timer_id' => $advertisementTimerId,
        ];

        return $this->makeCallGuzzle('DELETE', 'advertisement_timer', $body);
    }

    public function deleteAllAdvertisementTimers(int $userId, int $advertisementId)
    {
        $body = [
            'user_id'          => $userId,
            'advertisement_id' => $advertisementId,
        ];

        return $this->makeCallGuzzle('DELETE', 'advertisement_timers', $body);
    }
}
