<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait BalanceService
{
    public function getAllUserBalances(int $userId)
    {
        $body = [
            'user_id' => $userId,
        ];

        return $this->makeCallGuzzle('GET', 'user_all_balances', $body);
    }

    public function getTotalWithFrozenWalletsBalanceInBTC(int $account_id)
    {
        $body = [
            'account_id'  => $account_id
        ];

        return $this->makeCallGuzzle('GET', 'total_with_frozen_in_btc', $body);
    }

    public function getUserBalance(int $userId, int $assetId)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
        ];

        return $this->makeCallGuzzle('GET', 'user_balance', $body);
    }

    public function setFrozenBalance(int $userId, int $assetId, $amount, string $type = null)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
            'amount'   => $amount,
            'type'     => $type,
        ];

        return $this->makeCallGuzzle('PUT', 'set_frozen_balance', $body);
    }

    public function increaseBalance(int $assetId, int $userId, float $amount, string $type = null)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
            'amount'   => $amount,
            'type'     => $type,
        ];

        return $this->makeCallGuzzle('PUT', 'increase_balance', $body);
    }

    public function freezeBalance(int $assetId, int $userId, float $amount)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
            'amount'   => $amount,
        ];

        return $this->makeCallGuzzle('PUT', 'freeze_balance', $body);
    }

    public function restoreBalance(int $assetId, int $userId, float $amount)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
            'amount'   => $amount,
        ];

        return $this->makeCallGuzzle('PUT', 'restore_balance', $body);
    }
}
