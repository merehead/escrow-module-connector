<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait AdvertisementService
{
    public function getUserAdvertisements(
        int $userId,
        ?string $type = null,
        ?int $assetId = null,
        ?int $page = 1,
        ?int $perPage = 15
    )
    {
        $body = [
            'user_id'  => $userId,
            'type'     => $type,
            'asset_id' => $assetId,
            'page'       => $page,
            'per_page'   => $perPage,
        ];

        return $this->makeCallGuzzle('GET', 'user_advertisements', $body);
    }

    public function getActiveAdvertisements(
        int $countryId,
        ?int $userId = null,
        ?string $type = null,
        ?int $assetId = null,
        ?int $page = 1,
        ?int $perPage = 15,
        ?string $black_list = ''
    )
    {
        $body = [
            'country_id' => $countryId,
            'user_id'    => $userId,
            'type'       => $type,
            'asset_id'   => $assetId,
            'page'       => $page,
            'per_page'   => $perPage,
            'black_list' => $black_list,
        ];

        return $this->makeCallGuzzle('GET', 'advertisements', $body);
    }

    public function getAllAdvertisements(?int $page = 1, ?int $perPage = 15)
    {
        $body = [
            'page'       => $page,
            'per_page'   => $perPage,
        ];

        return $this->makeCallGuzzle('GET', 'all_advertisements', $body);
    }

    public function getAdvertisement(int $advertisementId, ?int $userId = null)
    {
        $body = [
            'advertisement_id' => $advertisementId,
            'user_d'           => $userId,
        ];

        return $this->makeCallGuzzle('GET', 'advertisement', $body);
    }

    public function postAdvertisement(array $body)
    {
        return $this->makeCallGuzzle('POST', 'advertisement', $body);
    }

    public function deleteAdvertisement(int $advertisementId, int $userId)
    {
        $body = [
            'advertisement_id' => $advertisementId,
            'user_id'          => $userId,
        ];

        return $this->makeCallGuzzle('DELETE', 'advertisement', $body);
    }

    public function changeActiveStatusAdvertisement(int $advertisementId, int $userId, bool $activeStatus)
    {
        $body = [
            'advertisement_id' => $advertisementId,
            'user_id'          => $userId,
            'active_status'    => $activeStatus,
        ];

        return $this->makeCallGuzzle('PUT', 'change_active_status_advertisement', $body);
    }

    public function changeUseTimersStatusAdvertisement(int $advertisementId, int $userId, bool $useTimersStatus)
    {
        $body = [
            'advertisement_id'  => $advertisementId,
            'user_id'           => $userId,
            'use_timers_status' => $useTimersStatus,
        ];

        return $this->makeCallGuzzle('PUT', 'change_use_timers_status_advertisement', $body);
    }

    public function getQuickAdvertisements(int $userId, int $countryId, int $assetId, string $type, ?float $amount = null)
    {
        $body = [
            'user_id'    => $userId,
            'country_id' => $countryId,
            'asset_id'   => $assetId,
            'type'       => $type,
            'amount'     => $amount,
        ];

        return $this->makeCallGuzzle('GET', 'advertisements_quick', $body);
    }
}
