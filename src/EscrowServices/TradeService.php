<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait TradeService
{
    public function getAllTrades(?int $page = 1, ?int $perPage = 15)
    {
        $body = [
            'page'     => $page,
            'per_page' => $perPage,
        ];

        return $this->makeCallGuzzle('GET', 'trades', $body);
    }

    public function getTrade(int $userId, int $tradeId)
    {
        $body = [
            'user_id'  => $userId,
            'trade_id' => $tradeId,
        ];

        return $this->makeCallGuzzle('GET', 'trade', $body);
    }

    public function createTrade(array $body)
    {
        return $this->makeCallGuzzle('POST', 'trade', $body);
    }

    public function cancelTrade(int $tradeId, int $userId)
    {
        $body = [
            'trade_id' => $tradeId,
            'user_id'  => $userId,
        ];

        return $this->makeCallGuzzle('PUT', 'cancel_trade', $body);
    }

    public function getUserOpenTrades(int $userId, ?int $assetId = null, ?string $type = null)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
            'type'     => $type,
        ];

        return $this->makeCallGuzzle('GET', 'users_open_trades', $body);
    }

    public function getUserClosedTrades(int $userId, ?int $assetId = null, ?string $type = null)
    {
        $body = [
            'user_id'  => $userId,
            'asset_id' => $assetId,
            'type'     => $type,
        ];

        return $this->makeCallGuzzle('GET', 'users_closed_trades', $body);
    }

    public function havePaidSeller(int $tradeId, int $userId)
    {
        $body = [
            'trade_id' => $tradeId,
            'user_id'  => $userId,
        ];

        return $this->makeCallGuzzle('PUT', 'have_paid_seller', $body);
    }

    public function haveReceivedPayment(int $tradeId, int $userId)
    {
        $body = [
            'trade_id' => $tradeId,
            'user_id'  => $userId,
        ];

        return $this->makeCallGuzzle('PUT', 'have_received_payment', $body);
    }

    public function openDispute(int $tradeId, int $userId)
    {
        $body = [
            'trade_id' => $tradeId,
            'user_id'  => $userId,
        ];

        return $this->makeCallGuzzle('PUT', 'open_dispute', $body);
    }

    public function getDisputeTrades(?int $page = 1, ?int $perPage = 15)
    {
        $body = [
            'page'     => $page,
            'per_page' => $perPage,
        ];

        return $this->makeCallGuzzle('GET', 'dispute_trades', $body);
    }

    public function getDisputeTradesByUser(int $userId, ?int $page = 1, ?int $perPage = 15)
    {
        $body = [
            'user_id'  => $userId,
            'page'     => $page,
            'per_page' => $perPage,
        ];

        return $this->makeCallGuzzle('GET', 'user_dispute_trades', $body);
    }

    public function getDisputeTrade(int $tradeId)
    {
        $body = [
            'trade_id' => $tradeId,
        ];

        return $this->makeCallGuzzle('GET', 'dispute_trade', $body);
    }

    public function approveTrade(int $tradeId)
    {
        $body = [
            'trade_id' => $tradeId,
        ];

        return $this->makeCallGuzzle('PUT', 'approve_trade', $body);
    }

    public function rejectTrade(int $tradeId)
    {
        $body = [
            'trade_id' => $tradeId,
        ];

        return $this->makeCallGuzzle('PUT', 'reject_trade', $body);
    }
}
