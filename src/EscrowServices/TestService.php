<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait TestService
{
    public function test($testParam)
    {
        $body = [
            'test_param' => $testParam,
        ];

        return $this->makeCallGuzzle('GET', 'test', $body);
    }
}
