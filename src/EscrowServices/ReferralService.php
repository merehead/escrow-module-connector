<?php

namespace MereHead\EscrowModuleConnector\EscrowServices;

trait ReferralService
{
    public function getAllReferrals(int $user_id)
    {
        $body = [
            'user_id' => $user_id,
        ];

        return $this->makeCallGuzzle('GET', 'get_all_referrals', $body);
    }
}
