<?php

namespace MereHead\EscrowModuleConnector\Modules;

use MereHead\EscrowModuleConnector\EscrowServices\{AdvertisementService,
    AdvertisementTimerService,
    BankService,
    ProfitService,
    ReferralService,
    TestService,
    TradeService,
    UserService,
    AssetsService,
    BalanceService};

/**
 * This class using for sending data to server
 * Class EscrowModuleService
 * @package Modules
 */
class EscrowModuleService extends EscrowConnectionModule
{
    use AssetsService,
        UserService,
        BalanceService,
        AdvertisementService,
        TestService,
        TradeService,
        AdvertisementTimerService,
        BankService,
        ProfitService,
        ReferralService;

    /**
     * Command for listening : ping
     * Pinging trade module server
     * @return mixed
     */
    public function ping()
    {
        $msg = [
            'command' => 'ping',
        ];

        return $this->makeCall($msg);
    }

}
