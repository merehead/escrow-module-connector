<?php

namespace MereHead\EscrowModuleConnector\Modules;

use function Couchbase\defaultDecoder;
use GuzzleHttp\Client;
use Illuminate\Encryption\Encrypter;

/**
 * Class EscrowConnectionModule
 * @package App\Services\Modules
 */
class EscrowConnectionModule
{
    const ENCRYPTER_KEY = 'AES-256-CBC';

    protected $requester;

    //default connection to Escrow-module
    protected $encrypter;

    protected $guzzleClient;

    //check is connected
    protected $connected = false;

    function __construct()
    {
        if (config('EscrowModuleConnector.encryption_key')) {
            $this->encrypter = new Encrypter(config('EscrowModuleConnector.encryption_key'), self::ENCRYPTER_KEY);
        }

        $this->guzzleClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('EscrowModuleConnector.escrowModuleDns'), //'http://localhost:82/api/',
        ]);
    }

    public function makeCallGuzzle(string $method, string $url, array $data)
    {
        $body = [];
        $data = json_encode($data);
        if (config('EscrowModuleConnector.encryption_key')) {
            $data = $this->encrypter->encrypt($data);
        }

        $body['encrypted_data'] = $data;

        try {
            $response = $this->guzzleClient->request(
                $method,
                '/api/' . $url,
                [
                    'headers' => ['content-type' => 'application/json'],
                    'body'    => json_encode($body),
                ]
            );

            return json_decode($response->getBody(), 1);
        } catch (\Exception $e) {
            $response = $e->getResponse();

            if (null !== $response) {
                $res = $response->getBody()->getContents();
                //Cheat to forward exceptions to main site
                if (env('APP_DEBUG')) {
                    dd($res);
                }

                dd('error in escrow module');
            }
        }
    }
}
