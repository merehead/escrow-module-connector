<?php

namespace MereHead\EscrowModuleConnector;

use Illuminate\Support\ServiceProvider;
use MereHead\EscrowModuleConnector\Modules\EscrowModuleService;

class ModuleServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $config = __DIR__ . '/Config/config.php';

        $this->publishes([
            $config => config_path('EscrowModuleConnector.php'),
        ], 'config');

        $this->mergeConfigFrom($config, 'EscrowModuleConnector');
    }

    /**
     * @throws \Exception
     */
    public function boot()
    {
        $this->app->bind('escrowmodule', EscrowModuleService::class);
    }
}
