<?php

namespace MereHead\EscrowModuleConnector\Helpers\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Facade who using for connecting to trade module
 * Class EscrowModule
 * @package Helpers\Facades
 */
class EscrowModule extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'escrowmodule';
    }
}
