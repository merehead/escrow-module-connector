# merehead

#Installation
Require this package in Laravel project composer.json and run composer update
        
        require {
            .....
            .....
            
            "merehead/escrow-module-connector": "dev-master"
            
            .....
            .....
        }
        
After updating composer, add the service provider line at the begining of providers array in /config/app.php

    'providers' => [
            .........................................
            .........................................
            
            MereHead\EscrowModuleConnector\ModuleServiceProvider::class
            
            ...................
            ........................
      ]
      'aliases' => [
      
            .....................
            .....................
            
            'EscrowModule' => MereHead\EscrowModuleConnector\Helpers\Facades\EscrowModule::class,
      
            ......................
            ........................
      ]
# Edit config
 
If you want to edit config you need to run

    php artisan vendor:publish --provider="merehead/escrow-module-connector" --tag=config 

    php artisan vendor:publish

So config-file will be moved to /config/EscrowModuleConnector.php and can be edited as you want and changes will not be lost after composer update.